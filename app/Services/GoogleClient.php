<?php namespace App\Services;

use Google_Client;

class GoogleClient
{
    private $google_client;
    public function __construct()
    {
        $this->google_client = new Google_Client();
        $this->google_client->setClientId('1003317324562-4la0tglej08onl5gu1p6skaqs6hh7slo.apps.googleusercontent.com');
        $this->google_client->setClientSecret('b7U_fpIk20gxxDSDchjOWJKh');
        $this->google_client->setRedirectUri(base_url().'/auth/google_login');
        $this->google_client->addScope('email');
        $this->google_client->addScope('profile');
    }
    public function getGoogleClient()
    {
        return $this->google_client;
    }

}