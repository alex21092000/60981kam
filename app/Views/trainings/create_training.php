<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('trainings/storeTraining'); ?>
        <div class="form-group">
            <label for="name">Название</label>
            <input type="text" class="form-control <?= ($validation->hasError('training_name')) ? 'is-invalid' : ''; ?>" name="training_name"
                   value="<?= old('training_name'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('training_name') ?>
            </div>

        </div>
        <div class="form-group">
            <label for="name">Дата</label>
            <input type="date" class="form-control <?= ($validation->hasError('date')) ? 'is-invalid' : ''; ?>" name="date"
                   value="<?= old('date'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('date') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="birthday">Изображение</label>
            <input type="file" accept=".jpg, .jpeg, .png, .bmp, .tif, .gif" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
            <div class="invalid-feedback">
                <?= $validation->getError('picture') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>