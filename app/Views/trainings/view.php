<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main" style="max-width: 1200px;">
        <div class="d-flex flex-column jumbotron">

            <?php if(!empty($sets)) : ?>
                <div class="d-flex">
                    <div class="set font-weight-bold" style="flex-basis: 10%;">Подход</div>
                    <div class="set font-weight-bold" style="flex-basis: 20%;">Упражнение</div>
                    <div class="set font-weight-bold" style="flex-basis: 12%;">Повторы</div>
                    <div class="set font-weight-bold" style="flex-basis: 12%;">Вес</div>
                    <div class="set font-weight-bold" style="flex-basis: 12%;">Отдых</div>
                    <div class="set font-weight-bold" style="flex-basis: 12%;border: none;"></div>
                    <div class="set font-weight-bold" style="flex-basis: 12%;border: none;"></div>
                </div>
                <?php foreach($sets as $i => $set) :?>
                    <div class="d-flex">
                        <div class="set font-weight-bold" style="flex-basis: 10%;"><?= $i ?></div>
                        <div class="set" style="flex-basis: 20%;"><?= esc($set['exercise'])  ?></div>
                        <div class="set" style="flex-basis: 12%;"><?= esc($set['reps']) ?></div>
                        <div class="set" style="flex-basis: 12%;"><?= esc($set['weight']) ?> кг</div>
                        <div class="set" style="flex-basis: 12%;"><?= esc($set['rest_time']) ?></div>
                        <div class="set" style="flex-basis: 12%;"><a href="<?= base_url()?>/trainings/editSet/<?= esc($set['id']); ?>">Редактировать</a></div>
                        <div class="set" style="flex-basis: 12%;"><a href="<?= base_url()?>/trainings/deleteSet/<?= esc($set['id']); ?>">Удалить</a></div>
                    </div>
                <?php endforeach; ?>
            <?php else: ?>
                <p>Список подходов пуст.</p>
            <?php endif;  ?>
            <a href="<?= base_url()?>/trainings/createSet/<?= esc($id_training); ?>" class="set btn btn-primary">Добавить</a>
        </div>
    </div>
<?= $this->endSection() ?>