<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container" style="max-width: 540px;">

        <?= form_open_multipart('trainings/storeSet');?>
        <input type="hidden" name="id_training" value="<?= esc($id_training); ?>">

        <div class="form-group">
            <label for="name">Упражнение</label>
            <input type="text" class="form-control <?= ($validation->hasError('exercise')) ? 'is-invalid' : ''; ?>" name="exercise"
                   value="<?= old('exercise'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('exercise') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Количество повторов</label>
            <input type="text" class="form-control <?= ($validation->hasError('reps')) ? 'is-invalid' : ''; ?>" name="reps"
                   value="<?= old('reps'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('reps') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Вес</label>
            <input type="text" class="form-control <?= ($validation->hasError('weight')) ? 'is-invalid' : ''; ?>" name="weight"
                   value="<?= old('weight'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('weight') ?>
            </div>
        </div>

        <div class="form-group">
            <label for="name">Время отдыха в секундах</label>
            <input type="text" class="form-control <?= ($validation->hasError('rest_time')) ? 'is-invalid' : ''; ?>" name="rest_time"
                   value="<?= old('rest_time'); ?>">
            <div class="invalid-feedback">
                <?= $validation->getError('rest_time') ?>
            </div>
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-primary" name="submit">Создать</button>
        </div>
        </form>


    </div>
<?= $this->endSection() ?>