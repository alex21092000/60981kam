<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>

<div class="container main" style="margin-top: 90px;">
    <?php use CodeIgniter\I18n\Time; ?>
    <h2>Все тренировки</h2>
    <?php if (!empty($trainings) && is_array($trainings)) : ?>
        <?php foreach ($trainings as $item) : ?>
            <div class="card mb-3" style="">
                <div class="row">

                    <div class="col-md-3 d-flex align-items-center">
                        <?php if (is_null($item['picture_url'])) : ?>
                            <img height="120" class="card-img" style="margin: 10px 0px;" src="/img/fitness.svg" alt="">
                        <?php else:?>
                            <img height="120" src="<?= esc($item['picture_url']); ?>" class="card-img">
                        <?php endif ?>
                    </div>

                    <div class="col-md-9 d-flex align-items-center">
                        <div class="card-body">
                            <h5 class="card-title"><?= esc($item['training_name']) ?></h5>
                            <p class="card-text"><?= esc(Time::parse($item['date'])) ?></p>
                            <a href="<?= base_url() ?>/index.php/trainings/view/<?= esc($item['id']) ?>" class="card-link btn btn-primary">Посмотреть</a>
                            <a href="<?= base_url() ?>/index.php/trainings/editTraining/<?= esc($item['id']) ?>" class="card-link btn btn-warning">Редактировать</a>
                            <a href="<?= base_url() ?>/index.php/trainings/deleteTraining/<?= esc($item['id']) ?>" class="card-link btn btn-danger">Удалить</a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else : ?>
        <p>Список тренировок пуст.</p>
    <?php endif ?>
</div>

<?= $this->endSection() ?>
