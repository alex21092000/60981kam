<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>


<div class="jumbotron text-center">
    <img class="mb-4" src="img/fitness.svg" alt="" width="72" height="72">
    <h1 class="display-4">Дневник тренировочных дней</h1>
    <p class="lead">Это приложение поможет создавать расписание тренировок.</p>
    <?php if (! $ionAuth->loggedIn()): ?>
        <a class="btn btn-primary btn-lg" href="<?=base_url()?>/auth/login" role="button">Войти</a>
    <?php else: ?>
        <p><a class="btn btn-primary btn-lg" href="<?=base_url()?>/trainings" role="button">Перейти к тренировкам</a></p>
    <?php endif ?>
</div>

<?= $this->endSection() ?>
