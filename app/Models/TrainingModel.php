<?php namespace App\Models;
use CodeIgniter\Model;
class TrainingModel extends Model
{
    protected $table = 'trainings';
    protected $allowedFields = ['id', 'training_name', 'date', 'user_id', 'picture_url'];
    public function getTrainings($id = null) {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
    public function getTrainingsByUserId($userId) {
        return $this->where(['user_id' => $userId])->findAll();
    }
    public function getTrainingWithUser($id = null, $search = '')
    {
        $builder = $this->select('*, trainings.id, trainings.picture_url')->join('users','trainings.user_id = users.id')->like('training_name', $search,'both', null, true);
        if (!is_null($id))
        {
            return $builder->where(['trainings.id' => $id])->first();
        }
        return $builder;
    }
}