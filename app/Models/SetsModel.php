<?php namespace App\Models;
use CodeIgniter\Model;
class SetsModel extends Model
{
    protected $table = 'training_set';
    protected $allowedFields = ['id', 'exercise', 'reps', 'weight', 'rest_time', 'id_training'];
    public function getSets($id = null) {
        if (isset($id)) {
            return $this->where(['id_training' => $id])->findAll();
        }
        else{
            return $this->findAll();
        }
    }
    public function getSet($id = null) {
        if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();
    }
}