<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addpictureurlfield extends Migration
{
	public function up()
	{
        if ($this->db->tableexists('trainings'))
        {
            $this->forge->addColumn('trainings',array(
                'picture_url' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => TRUE)
            ));
        }
	}

	public function down()
	{
        $this->forge->dropColumn('trainings', 'picture_url');
	}
}
