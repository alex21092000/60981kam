<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Trains extends Migration
{
	public function up()
	{
        if (!$this->db->tableexists('trainings'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'training_name' => array('type' => 'text', 'null' => FALSE),
                'date' => array('type' => 'date', 'null' => FALSE),
                'user_id' => array('type' => 'int', 'unsigned' => TRUE, 'null' => FALSE)
            ));
            $this->forge->addForeignKey('user_id', 'users','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('trainings', TRUE);

        }

        if (!$this->db->tableexists('training_set'))
        {
            // Setup Keys                                       
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'null' => FALSE, 'auto_increment' => TRUE),
                'exercise' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE),
                'reps' => array('type' => 'int', 'null' => FALSE),
                'weight' => array('type' => 'int', 'null' => FALSE),
                'rest_time' => array('type' => 'int', 'null' => FALSE),
                'id_training' => array('type' => 'int', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('id_training', 'trainings','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('training_set', TRUE);
        }
	}

	public function down()
	{
        $this->forge->droptable('trainings');
        $this->forge->droptable('training_set');
	}
}
