<?php namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class trains extends Seeder
{
    public function run()
    {
        $data = [

            'training_name' => 'Разминка',
            'date' => '2021-01-30',
            "user_id" => 1,
        ];
        $this->db->table('trainings')->insert($data);

        $data = [

            'training_name' => 'Заминка',
            'date' => '2021-01-31',
            "user_id" => 1,
        ];
        $this->db->table('trainings')->insert($data);

        $data = [

            'exercise' => 'Жим лежа',
            'reps' => 12,
            'weight' => 50,
            'rest_time' => 120,
            'id_training' => 7,
        ];
        $this->db->table('training_set')->insert($data);

        $data = [

            'exercise' => 'Подтягивания',
            'reps' => 10,
            'weight' => 0,
            'rest_time' => 60,
            'id_training' => 5,
        ];
        $this->db->table('training_set')->insert($data);

        $data = [

            'exercise' => 'Приседания',
            'reps' => 8,
            'weight' => 60,
            'rest_time' => 180,
            'id_training' => 7,
        ];
        $this->db->table('training_set')->insert($data);

        $data = [

            'exercise' => 'Становая тяга',
            'reps' => 4,
            'weight' => 80,
            'rest_time' => 240,
            'id_training' => 5,
        ];
        $this->db->table('training_set')->insert($data);



    }
}