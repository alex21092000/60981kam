<?php namespace App\Controllers;
use App\Models\TrainingModel;
use App\Models\SetsModel;
use Aws\S3\S3Client;

class Trainings extends BaseController
{
    public function index() //Обображение всех записей
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TrainingModel();
        if ($this->ionAuth->user()->row()->id == 1)
            return $this->viewAllWithUsers();
        $data ['trainings'] = $model->getTrainingsByUserId($this->ionAuth->user()->row()->id);
        echo view('trainings/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new SetsModel();
        $data ['sets'] = $model->getSets($id);
        $data['id_training'] = $id;
        echo view('trainings/view', $this->withIon($data));
    }
    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new TrainingModel();
            $data['trainings'] = $model->getTrainingWithUser(null, $search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('trainings/view_all_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Требуются права администратора'));
            return redirect()->to('/auth/login');
        }
    }

    public function createTraining()
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('trainings/create_training', $this->withIon($data));
    }

    public function storeTraining()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'training_name' => 'required|min_length[3]|max_length[255]',
                'date'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {

            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new TrainingModel();

            $data = [
                'training_name' => $this->request->getPost('training_name'),
                'date' => $this->request->getPost('date'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);

            return redirect()->to('/trainings');
        }
        else
        {
            return redirect()->to('/trainings/createTraining')->withInput();
        }
    }

    public function editTraining($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new TrainingModel();

        helper(['form']);
        $data ['training'] = $model->getTrainings($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('trainings/edit_training', $this->withIon($data));

    }

    public function updateTraining()
    {
        helper(['form','url']);
        echo '/trainings/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'id'  => 'required',
                'training_name' => 'required|min_length[3]|max_length[255]',
                'date'  => 'required',
                'picture'  => 'is_image[picture]|max_size[picture,1024]',
            ]))
        {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);

            }

            $model = new TrainingModel();

            $data = [
                'id' => $this->request->getPost('id'),
                'training_name' => $this->request->getPost('training_name'),
                'date' => $this->request->getPost('date'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);

            return redirect()->to('/trainings');
        }
        else
        {
            return redirect()->to('/trainings/editTraining/'.$this->request->getPost('id'))->withInput();
        }
    }

    public function deleteTraining($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

        $modelSets = new SetsModel();
        $setsOfTraining = $modelSets->where('id_training', $id)->findAll();
        foreach ($setsOfTraining as $set) {
            $modelSets->delete($set['id']);
        }

        $modelTrainings = new TrainingModel();
        $modelTrainings->delete($id);
        return redirect()->to('/trainings');
    }

    //-----------------------------SETS--------------------------------

    public function createSet($id_training)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data['validation'] = \Config\Services::validation();
        $data['id_training'] = $id_training;
        echo view('trainings/create_set', $this->withIon($data));
    }

    public function storeSet()
    {
        helper(['form','url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'exercise' => 'required|min_length[3]|max_length[255]',
                'reps'  => 'required|integer',
                'weight'  => 'required|integer',
                'rest_time'  => 'required|integer',
                'id_training'  => 'required',
            ]))
        {

            $model = new SetsModel();
            $model->save([
                'exercise' => $this->request->getPost('exercise'),
                'reps' => $this->request->getPost('reps'),
                'weight' => $this->request->getPost('weight'),
                'rest_time' => $this->request->getPost('rest_time'),
                'id_training' => $this->request->getPost('id_training'),
            ]);
            session()->setFlashdata('message', lang('Тренировка успешно создана'));
            return redirect()->to('/trainings/view/'.$this->request->getPost('id_training'));
        }
        else
        {
            return redirect()->to('/trainings/createSet/'.$this->request->getPost('id_training'))->withInput();
        }
    }

    public function deleteSet($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }

        $modelSets = new SetsModel();
        $thisSet = $modelSets->where(['id' => $id])->first();
        $training_id = $thisSet['id_training'];
        $modelSets->delete($id);
        return redirect()->to('/trainings/view/'.$training_id);
    }

    public function editSet($id)
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new SetsModel();

        helper(['form']);
        $data ['set'] = $model->getSet($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('trainings/edit_set', $this->withIon($data));

    }

    public function updateSet()
    {
        helper(['form','url']);
        echo '/trainings/edit/'.$this->request->getPost('id');
        if ($this->request->getMethod() === 'post' && $this->validate([
                'exercise'  => 'required',
                'reps'  => 'required|integer',
                'weight'  => 'required|integer',
                'rest_time'  => 'required|integer',
                'id_training'  => 'required',
            ]))
        {
            $model = new SetsModel();
            $model->save([
                'id' => $this->request->getPost('id'),
                'exercise' => $this->request->getPost('exercise'),
                'reps' => $this->request->getPost('reps'),
                'weight' => $this->request->getPost('weight'),
                'rest_time' => $this->request->getPost('rest_time'),
                'id_training' => $this->request->getPost('id_training'),
            ]);
            return redirect()->to('/trainings/view/'.$this->request->getPost('id_training'));
        }
        else
        {
            return redirect()->to('/trainings/editSet/'.$this->request->getPost('id'))->withInput();
        }
    }
}

